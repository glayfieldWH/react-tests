// app.js
import React from 'react'
import {Link, Route, Routes, useLocation} from 'react-router-dom'

import './App.css';

const About = () => <div>You are on the about page</div>
const Home = () => <div>You are home</div>
const NoMatch = () => <div>No match</div>

export const LocationDisplay = () => {
  const location = useLocation()

  return <div data-testid="location-display">{location.pathname}</div>
}

export const App = () => (
  <div className="App">
      <header className="App-header">
        <Link className="App-link" to="/">Home</Link>
        <Link className="App-link" to="/about">About</Link>
      </header>
    <Routes>
      <Route path="/" element={<Home />} />

      <Route path="/about" element={<About />} />

      <Route path="*" element={<NoMatch />} />
    </Routes>
    <footer>
      <LocationDisplay />
    </footer>
  </div>
)

export default App;