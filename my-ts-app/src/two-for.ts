const twoFor = (name?: string): string =>
	`One for ${name ?? "you"}, one for me.`

export { twoFor }