describe("App.js", () => {
  beforeAll(async () => {
    await page.goto("http://localhost:3000");
  });

  it("contains the welcome text", async () => {
    console.log('test running');
    await page.waitForSelector(".App-welcome-text");
    const text = await page.$eval(".App-welcome-text", (e) => e.textContent);
    expect(text).toContain("Edit src/App.js and save to reload.");
  });

});
